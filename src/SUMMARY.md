# Summary

[この文章について](about.md)
[はじめに](intro.md)

- [力学系](dynamical_systems/intro.md)
  - [状態と軌道](dynamical_systems/state.md)
  - [線形力学系](dynamical_systems/linear.md)
  - [アトラクター](dynamical_systems/nonlinear.md)
  - [軌道不安定性](dynamical_systems/instability.md)
  - [リアプノフ解析](dynamical_systems/lyapunov.md)

- [情報幾何学](information_geometry/intro.md)
  - [サイコロの確率論](information_geometry/measure.md)
  - [確率測度](information_geometry/lebesgue.md)
  - [指数型関数族](information_geometry/exponential_family.md)
  - [接続と測地線](information_geometry/geometry.md)

- [データ同化](data_assimilation/intro.md)
  - [観測のモデル化](data_assimilation/observation.md)
  - [運動法則のモデル化](data_assimilation/eom.md)
  - [カルマンフィルター](data_assimilation/kalman_filter.md)
  - [状態空間モデル](data_assimilation/graphical_model.md)
  - [粒子フィルター](data_assimilation/sampling.md)
  - [随伴法](data_assimilation/adjoint.md)
  - [拡張カルマンフィルター](data_assimilation/ekf.md)
  - [アンサンブルカルマンフィルター](data_assimilation/enkf.md)

- [補遺](appendix/intro.md)
  - [確率論](appendix/probability.md)
  - [関数空間](appendix/function_space.md)
  - [汎関数](appendix/functional.md)
