確率測度
========

\\(
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}
\newcommand\N{\mathbb{N}}
\newcommand\Riemann{\text{Riemann}}
\\)サイコロの場合の事象の空間 \\(X\\)  を実数 \\(\R\\)、あるいはユークリッド空間 \\(\R^N\\) の場合に拡張しておきましょう。
ここでは[Rene L. Schilling, "Measures, Integrals and Martingales"](https://doi.org/10.1017/CBO9780511810886)に従って測度・ルベーグ積分を導入する。
証明を議論するのはこの本の目的では無いので省く。

測度
----
実数に対して確率測度を部分集合に対する関数として考える際には少し難しい問題があります。
大雑把にいえば前節の離散的な場合のように全ての部分集合に対して測度を定義するのが困難なため
(困難性については測度論のテキストを参照してください)、
測度が定義出来る集合を限定します。

### σ加法族
\\(X\\) の部分集合族 \\(\mathcal{A}\\) で以下を満たすものをσ加法族と呼ぶ
\\[
X \in \mathcal{A}, \\\\
A \in \mathcal{A} \Rightarrow A^c \in \mathcal{A}, \\\\
(A_n)_{n \in \N} \Rightarrow \cup A_n \in \mathcal{A}
\\]

このσ加法族は次の性質を満たす

\\[
\emptyset \in \mathcal{A}, \\\\
(A_n)_{n \in \N} \Rightarrow \cap A_n \in \mathcal{A}
\\]

また任意の部分集合族 \\(P \subset 2^X\\)に対して最小のσ加法族が存在する。
このσ加法族を\\(\sigma(P)\\)と書く。
特にユークリッド空間 \\(\R^n\\) に対して、
その全ての開集合全体 \\(\mathcal{O}\\) から生成されるσ加法族を **Borel-σ加法族** と呼び
\\(\mathcal{B}(\R^n) = \sigma(\mathcal{O})\\)と書く。

このように補集合と可算個の和を取る演算に対して閉じている集合族を考えれば
測度を定義するのに十分な事が知られている。

### (正値)測度
\\(\mathcal{A}\\) を \\(X\\) におけるσ加法族とし、
\\( \mu: \mathcal{A} \to [0, \infty] \\) が次を満たすとき \\(\mu\\) を (正値)測度と呼ぶ

\\[
\mu(\emptyset) = 0, \\\\
(A_n)_{n \in \N} \subset \mathcal{A}, \text{pairwise disjoint}
\Rightarrow \mu\left( \cup A_n \right) = \sum \mu(A_n)
\\]

また定義域がσ加法族にならない場合は前測度と呼ぶ。
\\((X, \mathcal{A}, \mu)\\)を合わせて測度空間と呼ぶ。
特に \\(\mu(X) = 1\\) の時 **確率測度** と呼ぶ
測度が有限な\\(X\\) への増大列

\\[
(A_n)_{n \in \N}, \mu(A_n) < \infty, A_n \uparrow X
\\]

がとれるものをσ有限な測度と言う。

### ルベーグ測度
任意の半区間 \\([[a, b)) =[a_1, b_1) \times \ldots \times [a_n, b_n) \subset \R^n \\) に対して
\\[
\lambda_n([[a, b))) = \prod_{i=1}^n (b_i - a_i)
\\]
で定義される前測度 \\( \lambda_n \\) をルベーグ前測度と呼ぶ。
この前測度がBorel-σ加法族に一意に拡張できる事の証明は測度論のテキストに譲り、
ここでは認めることにする。

\\(B \subset \R^n\\) のルベーグ測度は以下のように平行移動 \\(x \in \R^n\\)、
回転 \\(R \in SO(n)\\) に対して不変であり、任意の線形変換 \\(M\\) に対してはその行列式で変化する

\\[
\begin{align}
  \lambda^n(x + B)     &= \lambda^n(B), \\\\
  \lambda^n(R^{-1}(B)) &= \lambda^n(B), \\\\
  \lambda^n(M^{-1}(B)) &= |\det M|^{-1} \lambda^n(B)
\end{align}
\\]

このようにルベーグ測度は面積や体積の自然な拡張となっている。
\\(\lambda^n(\R^N) = \infty\\) なのでこれは確率測度にはならない。

ルベーグ積分
------------
測度空間 \\((X, \mathcal{A}, \mu)\\) に対して、リーマン積分とは別の方法で積分を定義する。
\\(A \in \mathcal{A}\\)に対して定義関数
\\[
\chi_A(x) = \begin{cases}
  1 & x \in A \\\\
  0 & \text{otherwise}
\end{cases}
\\]
の積分をその測度の値で定義します
\\[
\int_X \chi_A d\mu = \mu(A)
\\]
続いて定義関数の有限和で書ける関数
\\(f(x) = \sum_{i=1}^M \alpha_i \chi_{A_i}(x) \\)
に対して線形性を使って拡張する
\\[
\int_X \sum_{i=1}^M \alpha_i \chi_{A_i} = \sum_{i=1}^M \alpha_i \mu(A_i)
\\]
互いに素で\\(X\\)全体をはる集合族 \\((A_i)_{i=1}^M \subset \mathcal{A}\\), i.e.

\\[
A_i \cap A_j = \emptyset, \text{for} i \neq j, 
X = \bigcup_{i=1}^M A_i
\\]

を使って

\\[
f(x) = \sum_{i=1}^M \alpha_i \chi_{A_i}(x)
\\]

のように書ける関数全体を \\(\mathcal{E}(\mathcal{A})\\) と書き **単関数** と呼ぶ。

### 可測関数

\\(u: X \to \R\\)が次を満たすとき **可測関数** と呼ぶ

\\[
u^{-1}(B) \in \mathcal{A}, \forall B \in \mathcal{B}(\R)
\\]
*sombreroの補題*:
正値可測関数 \\(u: X \to [0, \infty]\\) に対して各点収束する増大単関数列 \\(f_n \in \mathcal{E}(\mathcal{A}) \\) が存在する
\\[
u(x) = \sup_{n \in \N} f_n(x) = \lim_{n \to \infty} f_n(x), \\\\
f_1(x) \geq f_2(x) \geq \ldots
\\]
この増大列を使って正値可測関数\\(u\\) に対する積分を定義する:
\\[
\int_X u d\mu = \lim_{n \to \infty} \int_X f_n d\mu
\\]
一般の可測関数 \\(u\\) に対しては正値部分 \\(u_+\\) と負値部分 \\(u_-\\) に分けてそれぞれで近似列を定義してその差で定義する:
\\[
\int u d\mu = \int u_+ d\mu - \int u_- d\mu
\\]
また\\(x \in X\\)を明示的に書く記法として\\(u(x)\\)に対応して次のように書くことにする
\\[
\int u(x) \mu(dx)
\\]

### 絶対連続な測度
正値可測関数 \\(f: X \to [0, \infty]\\) に対して
\\[
\nu(A) = \int_A f d\mu
\\]
で\\(\nu\\)を定義するとこれも再び測度になる。一般に
\\[
N \in \mathcal{A}, \mu(N) \Rightarrow \nu(N) = 0
\\]
となる測度 \\(\nu\\) は \\(\mu\\) に対して絶対連続であると言い、\\(\nu \ll \mu\\) と書く。
\\(\mu\\)がσ有限の時、逆に \\(\nu \ll \mu\\) な測度に対して上の式を満たす \\(f\\) が存在する。
これをRadon-Nikodym導関数と呼び、次のように書く:
\\[
f = \frac{d\nu}{d\mu}
\\]
\\(\mu\\) がルベーグ測度 \\(\lambda^N\\) で \\(\nu\\)が確率測度の場合には
