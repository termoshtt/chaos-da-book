サイコロの確率論
=========

\\(
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}
\newcommand\Riemann{\text{Riemann}}
\\)まず素朴な確率論としてサイコロの場合から始めましょう。
取り得る値の空間を \\(X\\) と書き事象と呼びます:
\\[
X = \\{ 1, 2, 3, 4, 5, 6 \\}
\\]
サイコロを使ったゲームでは偶数 \\(X_E = \\{ 2, 4, 6 \\}\\) や
奇数 \\(X_O = \\{1, 3, 5\\}\\) といった、
この \\(X\\) 部分集合を考える必要があるので
全ての\\(X\\)の部分集合からなる集合 (冪集合と呼ぶ) \\(2^X\\) を考えます。
例えば偶数や奇数はこの要素となります \\(X_E, X_O \in 2^X\\)。
全事象 \\(X\\) や空事象 \\(\emptyset\\)、あるいは1つの目だけからなる集合 \\(\\{1\\}\\)  も\\(2^X\\) の要素となります。

サイコロを1回振って偶数が出る確率を \\(\mu(X_E)\\) と書きましょう。
確率は事象の組に対して定義されるので、
この \\(\mu\\) は事象の部分集合に対して実数を与える写像と考えられますね:
\\[
\mu : 2^X \to [0, 1]
\\]
この関数は次の性質を持つ事が知られています:
\\[
\mu(X) = 1, \mu(\emptyset) = 0
\\]
また2つの事象の組 \\(A, B \in 2^X\\) が \\(A \cap B = \emptyset\\) の時
\\[
\mu(A \cup B) = \mu(A) + \mu(B)
\\]
が成り立ちます。この関数 \\(\mu\\) を **確率測度** と呼びます。
より一般の確率測度の定義は次節で扱います。
このように事象全体とその部分集合、そして確率測度の組み \\((X, 2^X, \mu)\\) で
我々が直感的に考えているサイコロの確率論をモデル化していきます。

期待値と確率測度
-----------------
このサイコロでゲームをすることにしましょう。
\\(1\\) の目が出たら \\(10\\) 点、他の目が出たら \\(1\\) 点減点されるとしましょう。
この点数は \\(X\\) 上の関数として書けますね
\\[
f: X \to \R
\\]
(点数は便宜上実数にする) このゲーム \\(f\\) を１回行った時に得られる点数の期待値は、
それぞれの目が出る確率をかけて足し合わせるんでしたね
\\[
E_\mu[f] = \sum_{x \in X} f(x) \mu(\\{x\\})
\\]
この期待値を取る関数 \\(E_\mu\\) は関数を一つとって実数値を返す汎関数ですね。
しかも異なる二つのゲーム \\(f, g: X \to \R \\) に対して
\\[
E_\mu[f + g] = E_\mu[f] + E_\mu[g]
\\]
が成り立つ線形汎関数です。
\\(E_\mu\\) のように明示的に表示しているように、確率測度 \\(\mu\\) を定めると
期待値汎関数が1つ定まります。

では逆に期待値汎関数から確率測度が構成出来るでしょうか？
これは実は構成出来て、まず部分集合 \\(A \in 2^X\\) に対して **定義関数** と呼ばれる次の関数を導入します
\\[
\chi_A(x) = \begin{cases}
  1 & x \in A \\\\
  0 & \text{otherwise}
\end{cases}
\\]
この関数の期待値を計算すると
\\[
\begin{align}
E_\mu[\chi_A] &= \sum_{x \in X} \chi_A(x) \mu(\\{ x \\}) \\\\
              &= \sum_{x \in A} \mu(\\{ x \\}) \\\\
              &= \mu\left(\bigcup_{x \in A} \\{ x \\}\right) \\\\
              &= \mu(A)
\end{align}
\\]
上で述べた測度の和の規則を使っている事に注意します。
このように期待値汎関数 \\(E_\mu\\) から確率測度 \\(\mu\\) が再現できることが分かります。

確率測度の空間
-------------
\\(E_\mu\\) と \\(\mu\\) が一対一に定まる事を見ましたが、具体的にどうすれば定まるでしょうか？
\\(E_\mu\\) の定義から明らかなように各 \\(x \in X\\) に対して \\(\mu(\\{x\\})\\) を定めれば良いことがわかります。
つまり
\\[
S = \\{ p = (p_1, \ldots, p_6) \in \R^6 | p_x = \mu(\\{x\\}) \geq 0, \sum_{x \in X} p_x = 1  \\}
\\]
上の点と \\(E_\mu\\) すなわち \\(\mu\\) が一対一に対応します。
これをサイコロに対する可能な確率測度の空間と同一視することで問題を幾何的に捉えることができます。

例えば出る目の偏りが異なる２つのサイコロはこの空間上での異なる２点で表現され、
サイコロをふってみて偏りを調べる操作はこの空間上での推定問題として捉えられます。
サイコロ削りながら偏りを減らして一様な出目になるように調整する操作はこの空間での収束列を議論することに相当します。

この本の以降ではこの３つの表現、期待値汎関数として、確率測度として(部分集合に対する関数として)、パラメータ空間上の点としての
姿を自由に入れ替えて議論していきます。
