# この文章について


この文章は[GitLab][GitLab]上で開発されており継続的に更新されていきます。
現在はまだリリース前の段階であり、今後一定の完成度に到達した際にはリリース番号と
[zenode][zenode] によるDOIの付与が行われる予定です。
Qiitaや個別のPDFの配布によるノートの共有の方式は継続的に内容を
更新していく環境としてはあまり適しておらず、このように新たな本の書き方を模索する事になりました。
また本文中ではこれはあくまで本だという立場をとる事にします。

[GitLab]: https://gitlab.com/termoshtt/chaos-da-book
[zenode]: https://zenodo.org/

Contribution
------------
この本は下記のようにGNUフリー文書利用許諾契約書の元で配布されています。
タイポの指摘や内容に関する不明瞭な点の指摘などがあれば[GitLab/issue][issue]に報告してください。

[issue]: https://gitlab.com/termoshtt/chaos-da-book/issues

License
--------
Copyright (C) 2019 Toshiki Teramura

この文書を、フリーソフトウェア財団発行の [GNU フリー文書利用許諾契約書](https://gitlab.com/termoshtt/chaos-da-book/blob/master/LICENSE)(バージョン1.2かそれ以降から1つを選択)が定める条件の下で複製、頒布、あるいは改変することを許可する。変更不可部分、表カバーテキスト、裏カバーテキストは存在しない。
