力学系
=======

この章では力学系についての基本的な知識から始めてデータ同化の理解に必要な安定性/不安定性の概念まで解説していきます。
定義・定理・証明のような構成では行わず、数値実験例をベースに説明していきます。
数学的な側面を正確に議論したい場合は例えば以下のテキストを参照してください:

- "Nonlinear Oscillations, Dynamical Systems, and Bifurcations of Vector Fields"
  - John Guckenheimer, Philip Holmes

- "カオス力学系入門"
  - 原著者: Robert L. Devaney
  - 訳者: 後藤憲一、 新訂版訳者: 石井豊, 木坂正史, 国府寛司, 新居俊作

以下での議論はこれらの本の内容との整合性は特に想定していません。ご了承ください。
