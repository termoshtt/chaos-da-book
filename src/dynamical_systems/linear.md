線形力学系
==========

まず線形写像の場合の性質についてみてみましょう。
\\[
    x_{n+1} = A x_n
\\]
ここで\\(x_n \in \mathbb{R}^N \\)はN次元のベクトル、\\(A \in M_n(\mathbb{R})\\)は正方行列とします。
この時初期値\\(x_0\\)から開始した軌道は、各時刻\\(n\\)において
\\[
x_n = A^n x_0
\\]
のように行列のべき乗でかけます。この軌道は\\(A\\)の性質によってどのように変化するでしょうか？

1次元の場合
--------
簡単な例として\\(N=1\\)の場合を考えましょう。
この時\\(A = (a)\\)は成分が1つしかないスカラー量です。

まず初期値が\\(x_0 = 0\\)の場合を考えてみましょう。この時は\\(a\\)の値によらず\\( x_n = 0 \\)となりますね。
このように写像を適用しても動かない点の事を **固定点** と呼びます。
次元が大きくなっても原点 \\(O\\) は常に線形写像による力学系の固定点となります。

一方\\\(x_0 \neq 0 \\)の時、軌道は実数上の等比数列\\(x_0 a^n\\)となり、その漸近的な挙動は
\\[
\begin{align}
& \lim_{n\to \infty} |x_n| = 0      & (|a| < 1) \\\\
& \lim_{n\to \infty} |x_n| = |x_0|  & (|a| = 1) \\\\
& \lim_{n\to \infty} |x_n| = \infty & (|a| > 1)
\end{align}
\\]
のように\\(a\\)の絶対値が1より大きいか小さいかで決まります。
軌道が固定点に収束する場合(\\(|a|<0\\))にこの固定点を **安定固定点** と呼びます。
逆に固定点からどんどん離れていく場合(\\(|a|>1\\))、この固定点を **不安定固定点** と呼びます。

2次元以上の場合
---------------
次元が増えると安定な方向と不安定な方向が発生します。
まず2次元の場合で行列\\(A\\)が対角化されている場合を考えましょう：
\\[
A = 
\begin{pmatrix}
a_{11} & 0      \\\\
0      & a_{22} \\\\
\end{pmatrix}
\\]
するとこれは2つの成分\\(x_n = (y_n, z_n)\\)がそれぞれ独立に動く一次元の力学系のようにみえますね：
\\[
\begin{align}
y_{n+1} &= a_{11} y_n \\\\
z_{n+1} &= a_{22} z_n \\\\
\end{align}
\\]
この挙動は上で見た通り、それぞれ\\(a_{11}\\)と\\(a_{22}\\)が１より大きいか小さいかで決定します。
特に全ての成分について安定になるとき、固定点の事を **ノード(node)** と呼び、
安定と不安定な方向を両方持つ場合を **サドル(saddle)** 、
全ての成分に対して不安定な場合を **リペラー(repeller)** と呼びます。

この議論は行列\\(A\\)が対角化可能な場合はそのまま適用できます。
つまりあるユニタリー行列\\(U\\)があって、\\(U A U^t\\)が対角行列になる場合、
\\[
    U x_{n+1} = UAU^t Ux_n
\\]
が成り立つので\\(\hat{x}_n = Ux_n\\)として同じ議論が成り立ちます。

TODO: ジョルダン標準形の場合

時間依存する線形力学系
----------------------
前節で見たように、線形力学系の運動は行列の固有値で特徴付けられます。
では行列\\(A\\)が時刻によって変化する場合はどうなるでしょうか?
\\[
    x_{n+1} = A_n x_n
\\]
この時\\(x_0\\)から始まった軌道は
\\[
    x_n = A_{n-1} \cdots A_0 x_0
\\]
の様に書けます。ここで毎回\\(\cdots\\)で書くのはややこしいので、一般的な記法では無いですが時間順序積を導入しておきましょう
\\[
    x_n = A_{[n-1 \gets 0]} x_0
\\]
これで前節で\\(A^n\\)の固有値を見たように、\\(A_{[n \gets 1]}\\)の固有値を見れば良さそうですね！
行列\\(A\\)に対してその最大固有値をとる関数を\\(\sigma_{\text{max}}(A)\\)で書くと、極限
\\[
\lambda_\text{max} = \lim_{n\to \infty} \frac{1}{n} \log(\sigma_{\text{max}}(A_{[n \gets 1]}))
\\]
が存在するとき、この値が系の初期値鋭敏性を特徴づける量になります。大雑把に言うと
\\[
\| x_n \| \sim \lambda_\text{max}^n \| x_1 \|
\\]
のくらいのペースで指数的に大きくなっていきます。
\\(A_{[n \gets 1]} \\) の最大以外の固有値と固有ベクトルの扱いは少し厄介な問題となりますが、
非線形の場合の問題と合わせて議論することにしましょう。
