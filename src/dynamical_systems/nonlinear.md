アトラクター
============

線形でない関数\\( f(x) \\)による力学系を考えてみましょう。
線形力学系では状態\\(x\\)は大雑把に言って無限遠方に発散する、周期運動をする、あるいは原点に収束するの
3通りの挙動しか有りませんでしたが、一般の非線形の力学系ではこれらに加えて、有界な領域を運動し続ける
非周期的な運動が現れます。

ローレンツアトラクター
--------------------
ローレンツ方程式(データ同化の文脈ではもう1つあるので特にLorenz 63 model)と呼ばれる3変数の非線形微分方程式
\\[
\begin{align}
  \frac{dx}{dt} &= -p x + py \\\\
  \frac{dy}{dt} &= -xz + rx -y \\\\
  \frac{dz}{dt} &= xy - bz
\end{align}
\\]
は気象学者の[Edward Norton Lorenz][Lorenz]によって気象系のカオス的な振る舞いを示す簡単なモデルとして提出された方程式です。
3つのパラメータはそれぞれ\\(p = 10,r = 28,b = 8/3\\)の場合が良く使われ、今回もこの値を用いる。

この方程式を適当な初期値、例えば\\(x = 1.0, y = 0.0, z = 0.0\\)から開始すると、
有界な領域内を巡る非周期的な運動が実現される。

![Lorenz 63 Attractor](lorenz63.png)

これはNodeを持つ線形力学系で原点に全ての点が吸い込まれたのと同じように周囲の点を吸い込んでいくので
アトラクタ(Attractor)と呼ばれる。
