関数空間
========

\\(
\newcommand\R{\mathbb{R}}
\\)関数空間を微積分の知識と線形代数の知識を組み合わせることで定義してみましょう。
まず関数の足し算について議論します。
ある閉区間 \\(\Omega = [a, b] \subset \R \\) の上の連続な関数 \\(f, g \in C^0(\Omega)\\) を考えます。
この足し算 \\( f+g \in C^0(\Omega) \\) を次で定義します
\\[
(f+g)(x) = f(x) + g(x)
\\]
この左辺の括弧は、\\((f+g)\\) という関数の \\(x\\) での値をとっている事を強調していると思ってください。
同じように実数 \\(a \in \R\\) に対してスカラー倍 \\(af \in C^0(\Omega)\\) が定義できます。
この2つはまさに線形空間の公理ですので、連続関数全体 \\(C^0(\Omega)\\) は線形空間となります。
一般に何かしらの集合 \\(S\\) からベクトル空間
\\(V\\) への写像全体 \\(S \to V \\) に対して同じように線形空間の構造が入るのでこれらを **関数空間** と呼びます。
