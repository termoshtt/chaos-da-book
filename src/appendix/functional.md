汎関数
=======

\\(
\newcommand\R{\mathbb{R}}
\newcommand\C{\mathbb{C}}
\newcommand\Riemann{\text{Riemann}}
\\)次に汎関数を導入します。
ある閉区間 \\(\Omega = [a, b] \subset \R \\) の上の連続な関数 \\(f \in C^0(\Omega)\\) があるとします。
このリーマン積分を考えてみましょう：
\\[
\int_a^b f(x) dx
\\]
これは実数の値ですね。この値は関数 \\(f\\) によって決まっているので、
関数をとって、そのリーマン積分の値を返す関数 \\( \text{Riemann}: C^0(\Omega) \to \R \\)
を考えてみましょう
\\[
\Riemann(f) = \int_a^b f(x)dx
\\]
このように関数を引数にとって実数 (あるいは複素数\\(\C\\)) の値を返す関数を **汎関数** と呼びます。

さらにリーマン積分は2つのリーマン積分可能な関数 \\(f, g \in C^0(\Omega)\\) に対して次を満たします：
\\[
\int_a^b \left(f(x) + g(x)\right)dx = \int_a^b f(x) dx + \int_a^b g(x) dx
\\]
この性質は汎関数 \\(\Riemann\\) にとってどのようなものになるでしょうか？
上で議論した関数空間の性質から、上の関係式は次のように書けます：
\\[
\begin{align}
\Riemann(f + g) &= \int_a^b (f+g)(x) dx                 \\\\
                &= \int_a^b \left(f(x) + g(x)\right) dx \\\\
                &= \int_a^b f(x) dx + \int_a^b g(x) dx \\\\
                &= \Riemann(f) + \Riemann(g)
\end{align}
\\]
同じように \\(a \in \R\\) に対してスカラー倍, \\(af \in C^0(\Omega) \\) を定義すると
\\(\Riemann(af) = a\Riemann(f)\\) を満たします。
これはまさに関数空間の線形性を実数の線形性に対応させていることが分かり、
これら2つの性質を満たす汎関数を特に **線形汎関数** と呼びます。

他にどのような汎関数があるでしょうか？
ある点 \\( z \in \Omega \\) での関数の値をとる汎関数を考えてみましょう：
\\[
\delta_z(f) = f(z)
\\]
これが線形汎関数の性質を満たすことを確認してみてください：
\\[
\begin{align}
\delta_z(f + g) &= (f+g)(z) = f(z) + g(z) = \delta_z(f) + \delta_z(g) \\\\
\delta_z(af) &= (af)(z) = af(z) = a \delta_z (f)
\end{align}
\\]
もしデルタ関数の事を知っていれば、
これはデルタ関数を積分したものになっていることに気が付いたかもしれません
\\[
\delta_z(f) = \int_a^b f(x) \delta(x-z) dx
\\]
デルタ関数の定義はややこしいですが、このようにその積分だけを線形汎関数として定義するのはとても簡単です。
