はじめに
========

気象や海洋の運動のように非線形な時間発展を含む動的な状態を解析し理解するためには、
紙とペンを用いた手法では難しく、計算機が研究の主役となっています。
計算機を用いた系のシミュレーション、例えば量子化学系におけるハミルトニアンの固有値問題や偏微分方程式で記述される連続体の
初期値問題といった問題において新奇な現象や性質が観測され系の理解に役立ってきた一方で、
計算機の出力するデータ自体は何も語ってくれないためそれらを解釈するための理論的な枠組みも発達してきました。
この本ではその中でも系のダイナミクスに関する理論について数理的な側面を議論することが目的です。

Edward Norton Lorenzによる気象系の初期値に対する鋭敏性の発見は計算機科学の発展の歴史の中で重要な位置を占めます。
初期値問題において最初の状態のズレが時間の経過とともに増大していく軌道不安定性と呼ばれるこの性質は
現在ではリアプノフ指数として定量的に理解されています。
この性質により不安定な系ではいずれ初期値の情報を忘れてしまいますが、
実際に忘れるには非常に長い時間が必要であり、
予測や制御を行う時間スケールではこれらの情報は部分的に残っていることがわかります。
リアルタイムな系の部分観測による情報の取得と、
不安定性による情報の散逸を定量的に評価していく情報理論としてデータ同化と呼ばれる手法が主に気象の分野で発達してきました。
これらは系の予測や制御のような工学的な要求から生まれた学問ですが、
力学系における不安定性とベイズ統計的な状態推定の融合という理論的な側面から見ても非常に面白いものとなっています。
この理論的な側面をまとめることがこの本の目的です。
