#!/bin/bash
set -eux

for pdf in diagram/*.pdf; do
  name=$(basename $pdf)
  pdf2svg $pdf src/${name%.pdf}.svg
done
