カオスとデータ同化の数理
========================

https://termoshtt.gitlab.io/chaos-da-book/intro.html

License
-------
Copyright (C) 2019 Toshiki Teramura

この文書を、フリーソフトウェア財団発行の GNU フリー文書利用許諾契約書(バージョン1.2かそれ以降から一つを選択)が定める条件の下で複製、頒布、あるいは改変することを許可する。変更不可部分、表カバーテキスト、裏カバーテキストは存在しない。